variable "aws_ebs_csi_driver_helm_chart_version" {
  description = "The version of the helm chart."
  type        = string
  default     = "1.0.0"
}

variable "ebs_csi_driver_helm_values" {
  description = "Any additional Helm values to specify to AWS EBS CSI driver release."
  type        = list(string)
  default     = []
}
